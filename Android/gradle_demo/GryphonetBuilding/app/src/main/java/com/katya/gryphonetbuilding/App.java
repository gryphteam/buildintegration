package com.katya.gryphonetbuilding;

import android.app.Application;

import com.gryphonet.appright.AppRight;
import com.gryphonet.appright.LifeCyclePhase;

/**
 * Created by User on 09-09-16.
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        String accountId = "1b4218bc63d5b169d39e7c931cc6d4964ddbdc90";
        AppRight.start(this, accountId, LifeCyclePhase.DEVELOPMENT);
    }
}
