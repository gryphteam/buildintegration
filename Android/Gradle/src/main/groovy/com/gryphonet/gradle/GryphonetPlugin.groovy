package com.gryphonet.gradle

import com.android.build.gradle.api.ApplicationVariant
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import com.android.build.gradle.AppExtension
import com.android.build.gradle.AppPlugin

/**
 * Main gradle-gryphonet-plugin class
 */
class GryphonetPlugin implements Plugin<Project> {

    final static String GROUP_NAME = 'Gryphonet'

    void apply(Project project) {
        applyExtensions(project)
        applyTasks(project)
    }

    static void applyExtensions(final Project project) {
        project.extensions.create('gryphonet', GryphonetPluginExtension, project)
    }

    static void applyTasks(final Project project) {

        if (project.plugins.hasPlugin(AppPlugin)) {
            AppExtension android = project.android
            Task gryphonetUploadSymbolsTask = project.tasks.create("gryphonetUploadSymbols", Task);
            gryphonetUploadSymbolsTask.group = GROUP_NAME
            gryphonetUploadSymbolsTask.description = "Uploads all variants symbols to Gryphonet"
            gryphonetUploadSymbolsTask.outputs.upToDateWhen { false }
            String uploadAllPath = gryphonetUploadSymbolsTask.getPath();

            android.applicationVariants.all { ApplicationVariant variant ->
                GryphonetUploadTask task = project.tasks.create("upload${variant.name.capitalize()}ToGryphonet", GryphonetUploadTask)
                task.group = GROUP_NAME
                task.description = "Upload '${variant.name}' to Gryphonet"
                task.applicationVariant = variant
                task.variantName = variant.name
                task.outputs.upToDateWhen { false }
                task.dependsOn variant.assemble
                task.uploadAllPath = uploadAllPath

                gryphonetUploadSymbolsTask.dependsOn(task)
            }
        } else {
            project.task('gryphonetUploadSymbols', type: GryphonetUploadTask, group: GROUP_NAME)
        }
    }

}