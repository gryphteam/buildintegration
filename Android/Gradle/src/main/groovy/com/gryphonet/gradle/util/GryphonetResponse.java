package com.gryphonet.gradle.util;

import java.util.List;

/**
 * Created by tk on 09.09.16.
 */
public class GryphonetResponse {
    private int status;
    private List<String> request;

    public GryphonetResponse(int status, List<String> request) {
        this.status = status;
        this.request = request;
    }

    // getter
    public List<String> getRequest() { return request; }
    public int getStatus() { return status; }
}
