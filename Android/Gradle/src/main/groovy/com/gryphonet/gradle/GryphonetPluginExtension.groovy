package com.gryphonet.gradle

import org.gradle.api.Project

/**
 * Extension for plugin config properties
 */
class GryphonetPluginExtension {
    def Object outputDirectory
    def File symbolsDirectory = null
    def String gryphonetID = null
    def String manifestFile = "/app/src/main/AndroidManifest.xml"
    def Map<String, String> variantToGryphonetID = null
    def String mappingFileNameRegex = "mapping.txt"
    def String serverLink = "https://prod.gryphonet.com/app/api/symbols/for"
    def Map<String, String> variantToServerLink = null /* ["Debug": "https://stage.gryphonet.com/app/api/symbols/for", "Release": "https://prod.gryphonet.com/app/api/symbols/for"] */
    def int timeout = 60 * 1000

    private final Project project

    public GryphonetPluginExtension(Project project) {
        this.project = project
        this.outputDirectory = {
            return project.project.getBuildDir()
        }
    }
}
