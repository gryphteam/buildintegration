package com.gryphonet.gradle

import com.android.build.gradle.api.ApplicationVariant
import com.gryphonet.gradle.util.FileHelper
import com.gryphonet.gradle.util.GryphonetResponse
import com.gryphonet.gradle.util.MultipartUtility
import groovy.json.JsonSlurper
/*import org.apache.http.HttpHost
import org.apache.http.HttpResponse
import org.apache.http.HttpEntity
import org.apache.http.HttpStatus
import org.apache.http.entity.mime.HttpMultipartMode
import org.apache.http.client.HttpClient
import org.apache.http.client.config.RequestConfig
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.mime.MultipartEntityBuilder
import org.apache.http.entity.mime.content.FileBody
import org.apache.http.entity.mime.content.StringBody
import org.apache.http.impl.client.BasicResponseHandler
import org.apache.http.impl.client.HttpClientBuilder*/
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

/**
 * Upload task for plugin
 */
class GryphonetUploadTask extends DefaultTask {

    File mappingFile
    File manifestFile
    File symbolsDirectory
    String variantName
    ApplicationVariant applicationVariant
    GryphonetPluginExtension gryphonet
    String uploadAllPath
    String gryphonetID

    GryphonetUploadTask() {
        super()
        this.description = 'Uploads the symbol file (Android: (mapping.txt)) to Gryphonet'
    }

    @TaskAction
    def upload() throws IOException {

        gryphonet = project.gryphonet

        // Get the first output apk file if android
        if (applicationVariant) {

            if (applicationVariant.getMappingFile()?.exists()) {
                mappingFile = applicationVariant.getMappingFile()
            }
        }

        if (!getGryphonetID()) {
            throw new IllegalArgumentException("Cannot upload to Gryphonet because gryphonetID is missing")
        }

        // Retrieve mapping file if not using Android Gradle Plugin
        // Requires it to be set in the project config
        if (!mappingFile && gryphonet.symbolsDirectory?.exists()) {
            symbolsDirectory = gryphonet.symbolsDirectory
            mappingFile = FileHelper.getFile(gryphonet.mappingFileNameRegex, symbolsDirectory);
        }

        def manifest = new XmlSlurper().parse(project.projectDir.absolutePath + gryphonet.manifestFile)
        String versionCode = manifestVersion('versionCode')
        String versionName = manifestVersion('versionName')
        String packageName = manifest.@package.text()
        int appSignature = getAccountIdHash(gryphonetID)

        if (mappingFile) {
            logger.info("File found")
            uploadFilesToGryphonet(mappingFile, gryphonetID, versionCode, versionName, packageName, appSignature)
        } else {
            throw new IllegalStateException("Mapping file not found");
        }
    }

    def void uploadFilesToGryphonet(File mappingFile, String gryphonetID, String versionCode, String versionName, String packageName, int appSignature) {
                String charset = "UTF-8";
        String serverLink = getServerLink()
        String requestURL = "${serverLink}/${versionName}/${versionCode}/by/${gryphonetID}"

        MultipartUtility multipart = new MultipartUtility(requestURL, charset);

        multipart.addHeaderField("User-Agent", "CodeJava");
        multipart.addHeaderField("Test-Header", "Header-Value");

        multipart.addFormField("packageName", packageName);
        multipart.addFormField("signature", appSignature.toString());
        multipart.addFormField("hgcid", gryphonetID);

        multipart.addFilePart("fileUpload", mappingFile);

        GryphonetResponse response = multipart.finish();

        if (response.getStatus() == java.net.HttpURLConnection.HTTP_CREATED) {
            logger.lifecycle("Application uploaded successfully.")
            if (response.getRequest().size() > 0) {
                String responseString = response.getRequest().get(0);
                def uploadResponse = null
                try {
                    uploadResponse = new JsonSlurper().parseText('{' + responseString + '}')
                }
                catch (Exception e) {
                    logger.error("Error while parsing JSON response: " + e.toString())
                }
                if (uploadResponse) {
                    logger.lifecycle("Upload response: " + uploadResponse.message.toString())
                }
            }
        } else {
          parseResponseAndThrowError(response)
        }

        //System.out.println("SERVER REPLIED:");

        /*for (String line : response.getRequest()) {
            System.out.println(line);
        }*/

    }


    private void parseResponseAndThrowError(GryphonetResponse response) {
        if (response.getRequest().size() > 0) {
            String responseString = response.getRequest().get(0);
            Object uploadResponse = null
            try {
                uploadResponse = new JsonSlurper().parseText('{' + responseString + '}')
            } catch (Exception e) {
                println "Error while parsing JSON response: " + e.toString()
                logger.debug("Error while parsing JSON response: " + e.toString())
            }
            if (uploadResponse) {

                logger.debug("Upload response: " + uploadResponse.toString())
                if (uploadResponse.status && uploadResponse.status.equals("error") && uploadResponse.message) {
                    logger.lifecycle("Error response from Gryphonet: " + uploadResponse.message.toString())
                    throw new IllegalStateException("File upload failed: " + uploadResponse.message.toString() + " - Status: " + response.getStatus())
                }
            }
        }
        throw new IllegalStateException("File upload failed: status " + response.getStatus());
    }

    private String getGryphonetID() {
        gryphonetID = gryphonet.gryphonetID
        if (gryphonet.variantToGryphonetID) {
            if (gryphonet.variantToGryphonetID[variantName]) {
                gryphonetID = gryphonet.variantToGryphonetID[variantName]
            }
        }
        return gryphonetID
    }

    private String getServerLink() {
        String serverLink = gryphonet.serverLink
        if (gryphonet.variantToServerLink) {
            if (gryphonet.variantToGryphonetID[variantName]) {
                serverLink = gryphonet.variantToGryphonetID[variantName]
            }
        }
        return serverLink
    }

    private String manifestVersion(attribute) {
        def manifestFile = project.projectDir.absolutePath + gryphonet.manifestFile
        def ns = new groovy.xml.Namespace("http://schemas.android.com/apk/res/android", "android")
        def xml = new XmlParser().parse(manifestFile)
        if (attribute == 'versionName') {
            return xml.attributes()[ns.versionName].toString()
        } else {
            return xml.attributes()[ns.versionCode].toString()
        }
    }

    private int getAccountIdHash(accountID) {
        int count = accountID.length()
        int hash = 0
        if (count > 0) {
            for (int i = 0; i < count; ++i) {
                hash = 31 * hash + accountID.charAt(i)
            }
        }
        return hash
    }
}