package com.gryphonet.gradle.util

import org.gradle.api.Nullable

import java.util.regex.Pattern

class FileHelper {

    @Nullable
    def static getFile(String regex, File directory) {
        if (!regex || !directory || !directory.exists()) {
            return null
        }

        def pattern = Pattern.compile(regex)

        def fileList = directory.list(
                [accept: { d, f -> f ==~ pattern }] as FilenameFilter
        ).toList()

        if (!fileList) {
            return null
        }
        return new File(directory, fileList[0])
    }
}