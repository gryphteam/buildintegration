#!/usr/bin/env bash
cd $CONFIGURATION_BUILD_DIR
FILE=$($TARGET_NAME.app.dSYM)
if [ -f $FILE ]; then
    echo "Exist file"
    cp -r $CONFIGURATION_BUILD_DIR/$TARGET_NAME.app.dSYM/Contents $CONFIGURATION_BUILD_DIR
    cd $CONFIGURATION_BUILD_DIR
    zip -r Content.zip Contents
    rm -r Contents

    gryphonetID="" # set hashed Gryphonet ID
    serverLink="http://localhost:5000" # https://stage.gryphonet.com or https://prod.gryphonet.com

    buildNumber=$(/usr/libexec/PlistBuddy -c "Print CFBundleVersion" "${TARGET_BUILD_DIR}/${INFOPLIST_PATH}")
    versionNumber=$(/usr/libexec/PlistBuddy -c "Print CFBundleShortVersionString" "${TARGET_BUILD_DIR}/${INFOPLIST_PATH}")
    curl -i -X POST -H "Content-Type: multipart/form-data" -F "fileUpload=@Content.zip" -F "packageName=$TARGET_NAME" -F "hgcid=$gryphonetID" $serverLink/app/api/symbols/for/$buildNumber/$versionNumber/by/$gryphonetID
else
    echo "The File Does Not Exist"
fi